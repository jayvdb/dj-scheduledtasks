from django.test import TestCase
from django.utils import timezone
from example_app.models import Todo
from scheduledtasks.models import STATUS, ScheduledTask
from datetime import datetime, timedelta
from unittest import mock

from .testutils import create_scheduled_tasks
import pytz

class ScheduleTaskTestCase(TestCase):

    def setUp(self):
        time = timezone.now()
        self.task = ScheduledTask.schedule(
            'example_app.tasks.ping',
            args = ['foo'],
            time=time
        )

    def test_runs_sync_successfully(self):
        self.task.run_async = False
        self.task.run(force=True)
        self.assertEqual(
            self.task.status,
            STATUS.Success.value
        )

    @mock.patch('example_app.tasks.ping.apply_async')
    def test_can_run_async_successfully(self, mock_async_method):
        mock_async_method.return_value = 'hi!'
        self.task.run_async = True
        self.task.run(force=True)

        mock_async_method.assert_called()
        self.assertEqual(
            self.task.scheduledtaskrun_set.first().result,
            'hi!'
        )

class ScheduleObjectTaskTestCase(TestCase):

    def setUp(self):
        self.offset = 60
        self.todo = Todo.objects.create(title='test')
        self.task = ScheduledTask.schedule_by_object(
            self.todo,
            'example_app.tasks.ping',
            'due_time',
            offset = self.offset
        )

    def test_schedule_relative_to_time(self):

        self.assertEqual(
            self.todo.due_time + timedelta(minutes=self.offset),
            self.task.scheduled_time
        )

    def test_task_is_attached_to_instance(self):
        self.assertEqual(
            self.todo.scheduled_tasks.count(),
            1
        )

class ScheduleRunnerTestCase(TestCase):

    def setUp(self):
        schedule = [
            # these are stored as utc
            '2020-06-29 17:00',
            '2020-06-30 12:00',
            '2020-06-29 09:00',
            '2020-06-29 08:30',
            '2020-06-29 08:00'
        ]
        tasks = create_scheduled_tasks('example_app.tasks.ping', schedule)

    def test_run_tasks_negative_offset(self):
        time_string = '2020-06-29 09:05'
        reference_date = pytz.utc.localize(datetime.strptime(time_string, '%Y-%m-%d %H:%M'))
        tasks_to_run = ScheduledTask.get_scheduled_tasks(reference_date, window=-30)
        self.assertEqual(len(tasks_to_run), 1)
        self.assertEqual(
            tasks_to_run[0].scheduled_time.isoformat(),
            '2020-06-29T09:00:00+00:00'
        )

    def test_run_tasks_larger_negative_offset(self):
        time_string = '2020-06-29 09:05'
        reference_date = pytz.utc.localize(datetime.strptime(time_string, '%Y-%m-%d %H:%M'))
        tasks_to_run = ScheduledTask.get_scheduled_tasks(reference_date, window=-90)
        self.assertEqual(len(tasks_to_run), 3)


    def test_run_tasks_positive_offset(self):
        time_string = '2020-06-29 08:55'
        reference_date = pytz.utc.localize(datetime.strptime(time_string, '%Y-%m-%d %H:%M'))
        tasks_to_run = ScheduledTask.get_scheduled_tasks(reference_date, window=30)
        self.assertEqual(len(tasks_to_run), 1)
        self.assertEqual(
            tasks_to_run[0].scheduled_time.isoformat(),
            '2020-06-29T09:00:00+00:00'
        )

    def tearDown(self):
        ScheduledTask.objects.all().delete()



