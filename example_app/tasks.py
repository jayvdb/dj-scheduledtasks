from .models import Todo
from celery import shared_task

@shared_task
def ping(msg='pong', *args, **kwargs):
    return msg

def send_reminder(todo_id):
    todo = Todo.objects.get(id=todo_id)
    return todo
